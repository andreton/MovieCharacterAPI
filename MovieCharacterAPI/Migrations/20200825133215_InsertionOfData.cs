﻿using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Query.Internal;
using MovieCharacterAPI.Model;
using System;

namespace MovieCharacterAPI.Migrations
{
    public partial class InsertionOfData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            /*
            migrationBuilder.InsertData(
                table: "Movie",
                columns: new[] { "MovieTitle", "Genres", "ReleaseYear", "Director", "PictureUrl", "Trailer", "FranchiseId" },
                values: new object[,] { { "The fellowship of the ring", "Adventure", new DateTime(2001, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Peter Jackson", "https://www.imdb.com/title/tt0120737/mediaviewer/rm3592958976?ref_=tt_ov_i", "https://www.imdb.com/title/tt0120737/mediaviewer/rm3592958976?ref_=tt_ov_i", 1 },
                                        {"Batman and Robin", "Action", new DateTime(2001, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),"George Lucas","https://www.imdb.com/title/tt0120737/mediaviewer/rm3592958976?ref_=tt_ov_i", "https://www.youtube.com/watch?v=EXeTwQWrcwY", 5},
                                        {"The dark knight","Action",new DateTime(2008, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),"Christopher Nolan","https://en.wikipedia.org/wiki/The_Dark_Knight_(film)#/media/File:Dark_Knight.jpg", "https://www.imdb.com/title/tt0120737/mediaviewer/rm3592958976?ref_=tt_ov_i", 2}
        }
    }
            );

            migrationBuilder.DropPrimaryKey(
                name: "PK_MovieCharacter",
                table: "MovieCharacter");

            migrationBuilder.AddPrimaryKey(
                name: "PK_MovieCharacter",
                table: "MovieCharacter",
                columns: new[] { "MovieId", "CharacterId", "ActorId" });
            */
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_MovieCharacter",
                table: "MovieCharacter");

            migrationBuilder.AddPrimaryKey(
                name: "PK_MovieCharacter",
                table: "MovieCharacter",
                columns: new[] { "MovieId", "CharacterId" });
        }
    }
}
