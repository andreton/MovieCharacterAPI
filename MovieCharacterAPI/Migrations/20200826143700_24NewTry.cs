﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MovieCharacterAPI.Migrations
{
    public partial class _24NewTry : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(table: "Franchises",
               columns: new[] { "Name", "Description" },
               values: new object[,] { { "The lord of the rings ", "An epic fantasy saga, based on the books by JRR Tolkien" },
                                        { "Batman ", "The story about the masked vigilante" }});
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
