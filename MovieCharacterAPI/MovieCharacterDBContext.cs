﻿using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterAPI
{
    public class MovieCharacterDBContext : DbContext
    {
        public DbSet<Actor> Actors { get; set; }
        public DbSet<Character> Characters { get; set; }
        public DbSet<Movie> Movie { get; set; }
        public DbSet<MovieCharacter> MovieCharacter {get;set;}
        public DbSet<Franchise> Franchises { get; set; }

        public MovieCharacterDBContext(DbContextOptions<MovieCharacterDBContext> options) : base(options) { }
        /*
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Data Source=PC7372\SQLEXPRESS;Initial Catalog=MovieCharacter;Integrated Security=True");
        }
        */

        protected override void OnModelCreating(ModelBuilder modelbuilder)
        {
            modelbuilder.Entity<MovieCharacter>().HasKey(pq => new { pq.MovieId, pq.CharacterId,pq.ActorId });

            modelbuilder.Entity<MovieCharacter>()
                .HasOne(mc => mc.Character)
                .WithMany(c => c.MovieCharacters)
                .HasForeignKey(mc => mc.CharacterId);

            modelbuilder.Entity<MovieCharacter>()
               .HasOne(ma => ma.Actor)
               .WithMany(t => t.MovieCharacters)
               .HasForeignKey(aa => aa.ActorId);

            modelbuilder.Entity<MovieCharacter>()
                .HasOne(me => me.Movie)
                .WithMany(m => m.MovieCharacters)
                .HasForeignKey(mm => mm.MovieId);

           
        }
    }
}
